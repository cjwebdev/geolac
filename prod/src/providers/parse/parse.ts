import { Http, RequestOptions, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { ENV } from '../../app/app.constant';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';


/*
  Generated class for the ParseProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ParseProvider {
  private serverphp = ENV.parseServerUrl;
  private options = new RequestOptions({
    headers: new Headers({
      'Content-Type': 'application/json',
      'X-Parse-Application-Id': ENV.parseAppId,
      'X-Parse-REST-API-Key': ENV.parseAPI
    })
  });

  constructor(public http: Http) {
  }


  //Load lake
  public ObserveLake() {
    return this.http.get(this.serverphp + '/mylake').map((res) => res.json());
  }
  public promiseLake(obj) {
    let promise = new Promise((resolve, reject) => {
      this.ObserveLake().toPromise().then(res => {
        resolve(res);
      }).catch((data => {
        resolve(false);
      }));
    });
    return promise;
  }

  public activite(obj) {
    return this.http.post(this.serverphp + '/lake_activite', obj).map((res) => res.json());
  }
  public pactivite(obj) {
    let promise = new Promise((resolve, reject) => {
      this.activite(obj).toPromise().then(res => {
        resolve(res);
      }).catch((data => {
        resolve(false);
      }));
    });
    return promise;
  }
  //load Lake

  //Load Activite lake
  public ObserveActivite() {
    return this.http.get(this.serverphp + '/activite').map((res) => res.json());
  }
  public promiseActivite() {
    let promise = new Promise((resolve, reject) => {
      this.ObserveActivite().toPromise().then(res => {
        resolve(res);
      }).catch((data => {
        resolve(false);
      }));
    });
    return promise;
  }
  //load Activite lake


  //Load AllActivite lake
  public ObserveAllActivite() {
    return this.http.post(this.serverphp + '/functions/allactivitelake', {}, this.options).map((res) => res.json());
  }
  public promiseAllActivite() {
    let promise = new Promise((resolve, reject) => {
      this.ObserveAllActivite().toPromise().then(res => {
        resolve(res);
      }).catch((data => {
        resolve(false);
      }));
    });
    return promise;
  }
  //load Activite lake

  //Load Activite
  public ObserveActiviteListe() {
    return this.http.post(this.serverphp + '/functions/activiteliste', {}, this.options).map((res) => res.json());
  }
  public promiseActiviteListe() {
    let promise = new Promise((resolve, reject) => {
      this.ObserveActiviteListe().toPromise().then(res => {
        resolve(res);
      }).catch((data => {
        resolve(false);
      }));
    });
    return promise;
  }
  //load Activite


}
