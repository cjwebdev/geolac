import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { ENV } from '../../app/app.constant';

import 'rxjs/add/operator/map';
declare var google: any;

/*
  Generated class for the ExterneServiceProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ExterneServiceProvider {

  private serverphp = ENV.parseServerUrl;

  constructor(public http: Http) {
  }

  public searchPlace(map, latitude, longitude): Promise<any> {
    return new Promise((resolve, reject) => {
     
        var service = new google.maps.places.PlacesService(map);
        service.nearbySearch({
          location: { lat: Number(latitude), lng: Number(longitude) },
          radius: 40000
        }, (data) => {
          resolve(true);
        });
      
    });
  }

  public getImagePlaceId(id, latitude, longitude): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        let photo = []
        this.http.post(this.serverphp + '/google/place_id', {id:id}).map((res) => res.json())
          .subscribe((data) => {
            if (id != 'undefined' && data && data.result && data.result.photos) {
              data.result.photos.forEach((place, i) => {
                let photolac = this.serverphp + '/google/reference?reference=' + place.photo_reference;
                photo.push(photolac);
                if (i == data.result.photos.length - 1) {
  console.log(photo)
                  resolve(photo);
                }
              });
            } else {
              this.http.get(this.serverphp + '/google/position?longitude=' + longitude + '&latitude=' + latitude)
                .subscribe((data) => {
                  let dataa:any = data
                  photo.push(dataa.url);
                  resolve(photo);
                });
            }
          });
      }, 50);
    });
  }



}
