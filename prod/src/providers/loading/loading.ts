import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

/*
  Generated class for the LoadingProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LoadingProvider {
  private loader
  private present = false;
  constructor(public loading: LoadingController) {

  }

  show(text) {
    if (this.present) {
      this.hide()
    }

    if (text == '') {
      this.loader = this.loading.create({
        content: 'Initialisation des lacs..',
      });
    } else {
      this.loader = this.loading.create({
        content: text,
      });
    }
    this.present = true;
    this.loader.present();
  }

  hide() {
    if (this.present) {
      this.present = false;
      this.loader.dismiss();
    }
  }
}
