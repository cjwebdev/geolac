import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';


/*
  Generated class for the LocalisationProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class LocalisationProvider {
  public longitude;
  public latitude;
  constructor(private geolocation: Geolocation, private platform: Platform) {
  }

  public run(): Promise<any> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.geolocation.getCurrentPosition().then((resp) => {
          this.latitude = resp.coords.latitude
          this.longitude = resp.coords.longitude
          resolve(true)
        }).catch((error) => {
          resolve(false)

        });
      }, 500);
    });
  }
}
