import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';


/*
  Generated class for the ToastProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class ToastProvider {

  constructor(private toastCtrl: ToastController) {
  }

  show(message, duration, position) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: duration,
      position: position
    });
    toast.present();
  }

}
