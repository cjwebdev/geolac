import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchBarComponent } from './search-bar';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(SearchBarComponent),
  ],
  exports: [

  ]
})
export class SearchBarComponentModule {}
