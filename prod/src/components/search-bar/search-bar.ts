import { Component, Output, EventEmitter } from '@angular/core';

/**
 * Generated class for the SearchBarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'search-bar',
  templateUrl: 'search-bar.html'
})
export class SearchBarComponent {

  @Output()
  change: EventEmitter<number> = new EventEmitter<number>();
  constructor() {}


  onInput(event){
    this.change.emit(event)
  }

}
