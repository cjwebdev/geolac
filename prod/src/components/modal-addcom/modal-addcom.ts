// Angular
import { Component, Renderer } from '@angular/core';

// Ionic
import { ViewController, NavParams, NavController } from 'ionic-angular';

// Providers
import { ParseProvider } from '../../providers/parse/parse';
import { AuthProvider } from '../../providers/auth/auth';
import { SigninPage } from '../../pages/signin/signin';


@Component({
  selector: 'modal-addcom',
  templateUrl: 'modal-addcom.html'
})
export class ModalAddcomComponent {

  text: string;
  lake: any;
  constructor(private parseProvider: ParseProvider, public nav: NavController, public auth: AuthProvider, public renderer: Renderer, public viewCtrl: ViewController, public params: NavParams) {
    renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-addcom', true);
    this.lake = params.get('info');
  }

  // public postCom() {
  //   if (this.auth.authenticated()) {
  //     if (!this.text) {
  //       return alert('Impossible');
  //     }
  //     let obj = { name: this.auth.currentUser().id, text: this.text, lakeId: this.lake.objectId }
  //     this.parseProvider.addComent(obj).then((gameScore) => {
  //       this.viewCtrl.dismiss();
  //     }, (error) => {
  //       alert('Error adding score.');
  //     });
  //   } else {
  //     this.nav.push(SigninPage)
  //   }
  // }


}
