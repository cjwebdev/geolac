import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalAddcomComponent } from './modal-addcom';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ModalAddcomComponent),
  ],
  exports: [
  ]
})
export class ModalAddcomComponentModule {}
