import { Component, Input, ChangeDetectorRef, Renderer } from '@angular/core';
import { ModalController, NavParams, ViewController } from 'ionic-angular';

import { ExterneServiceProvider } from '../../providers/externe-service/externe-service';
import { SearchfilterPipe } from '../../pipes/searchfilter/searchfilter';
import { ModalContentComponent } from '../modal-content/modal-content';
// import { ModalWindowComponent } from '../modal-window/modal-window';
import { HomePage } from '../../pages/home/home';
/**
 * Generated class for the SearchListComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'search-list',
  templateUrl: 'search-list.html'
})
export class SearchListComponent {

  @Input() listLake: any;
  @Input() word: any;
  map: any;
  public filtre: any;
  public searchList;
  constructor(public renderer: Renderer, public externeService: ExterneServiceProvider, public home: HomePage, public modal: ModalController, private filterSearch: SearchfilterPipe, private cdRef: ChangeDetectorRef, public viewCtrl: ViewController, public params: NavParams) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'search-list', true);
    this.map = params.get('map')
    this.init()
  }

  ngOnChanges(event) {

    if (event.listLake && event.listLake.currentValue) {
      this.init()
    }
    if (event.word && event.word.currentValue && !event.word.currentValue.type) {
      this.search(event.word.currentValue);
    }
  }

  search(word) {
    this.searchList = this.filterSearch.transform(this.listLake, word);
    // this.img();
  }
  searchLake(lake) {

    // this.listLake.forEach(Lake => {
    //   let lakePos = {lat: Lake.latitude, lng: Lake.longitude};
    //   this.map.setCenter(lakePos);

    // });

    // let lakePs = {lat: this.listLake[''].latitude, lng: this.listLake[''].longitude}

    let lakePos = { lat: lake.loc[1], lng: lake.loc[0] };
    console.log(lakePos);

    let options = { center: lakePos, zoom: 14 };
    this.home.map.setOptions( options);
    this.modal.create(ModalContentComponent, { info: lake }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'contentmodal' }).present();
  }
  init() {
    this.search('');
  }

  // img() {
  //   this.searchList.forEach(element => {
  //     let resultRac = element.lake.attributes
  //     this.externeService.getImagePlaceId(resultRac.place_id, resultRac.latitude, resultRac.longitude).then((data) => {
  //       element.img_link = data
  //     })
  //   });
  // }

  openContent(lake) {
    this.modal.create(ModalContentComponent, { info: lake }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'contentmodal' }).present()
  }


}
