import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchListComponent } from './search-list';

@NgModule({
  declarations: [

  ],
  imports: [
    IonicPageModule.forChild(SearchListComponent),
  ],
  exports: [

  ]
})
export class SearchListComponentModule {}
