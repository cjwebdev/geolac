import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalContentComponent } from './modal-content';

@NgModule({
  declarations: [
    
  ],
  imports: [
    IonicPageModule.forChild(ModalContentComponent),
  ],
  exports: [
    
  ]
})
export class ModalContentComponentModule {}
