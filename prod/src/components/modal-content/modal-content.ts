import { Component, Renderer } from '@angular/core';
import { ViewController, NavParams, ModalController, NavController } from 'ionic-angular';
import { ExterneServiceProvider } from '../../providers/externe-service/externe-service';
import { ChangeDetectorRef } from '@angular/core';
import { ParseProvider } from '../../providers/parse/parse';
import { AuthProvider } from '../../providers/auth/auth';
import { SigninPage } from '../../pages/signin/signin';
import * as _ from 'lodash';
import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';

AuthProvider
declare var google: any;


@Component({
  selector: 'modal-content',
  templateUrl: 'modal-content.html'
})
export class ModalContentComponent {
  

  lake: any;
  img: any;
  objectKeys: any = Object.keys;
  activite;
  nombre = 0;
  tab = 'info';
  com: any;
  vote = 3;
  time = Math.floor(Math.random() * 5000);;
  json = [
    { name: "latitude", attribute: "loc", index: '0' },
    { name: "longitude", attribute: "loc", index: '1' },
    { name: "type", attribute: "type" },
    { name: "altitude", attribute: "altitude" },
    { name: "superficie", attribute: "superficie" },
    { name: "profondeur", attribute: "profondeur" }
  ];
  constructor(public navCtrl: NavController, private admobFree: AdMobFree, public auth: AuthProvider, private cf: ChangeDetectorRef, public parse: ParseProvider,
    public renderer: Renderer, public modal: ModalController, public externeService: ExterneServiceProvider, public viewCtrl: ViewController, public params: NavParams) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-content', true);
    this.lake = params.get('info');
    this.timeShow(this.time);


  }

  ngOnInit() {
    this.run();


  }
  timeShow(time) {
    setTimeout((time) => {
      console.log('pub')
        this.showInterstitialAds()
    }, time)
  }
  stopCount(time) {
    clearTimeout(time);
    time = 0;
    console.log('annuler')
  }
  showInterstitialAds() {

    const bannerConfig: AdMobFreeBannerConfig = {
      id: 'ca-app-pub-2580762866650596/3421855987',
      isTesting: false,
      autoShow: true
    };
    let timeShow = Math.floor(Math.random() * 10000);
    setTimeout((timeshow) => {

      this.admobFree.interstitial.config(bannerConfig)
      this.admobFree.interstitial.prepare()
        .then(() => {
          this.admobFree.interstitial.show()
        })
        .catch(e => console.log(e))


    }, timeShow);



  }
  close(time) {
    this.viewCtrl.dismiss();
    this.stopCount(time)

  }

  // rating(event) {
  //   if (this.auth.authehttps://fr.wikipedia.org/wiki/Les_Aillons-Marg%C3%A9riaznticated()) {
  //     this.cf.detectChanges();
  //     this.parse.addRating(this.lake.id, event).then((data) => {
  //       console.log('ok');
  //     })
  //   } else {
  //     this.navCtrl.push(SigninPage);
  //   }
  // }


  run() {
    this.com = [];
    this.externeService.getImagePlaceId(this.lake.place_id, this.lake.loc[1], this.lake.loc[0]).then((data) => {
      this.img = data;
      this.cf.detectChanges();
      this.cf.detectChanges();
    })

    this.parse.promiseActivite().then((data) => {
      let dataa: any = data;
      this.activite = dataa.data
    });

  }

  getIndex(obj, index) {
    return obj[index]
  }
  setPanorama(distance) {
    if (this.nombre == 4) {
      this.nombre = 0;
    }
    let pos = {
      lat: Number(this.lake.loc[1]),
      lng: Number(this.lake.loc[0])
    };
    let service = new google.maps.StreetViewService();
    service.getPanoramaByLocation(pos, distance, (result) => {
      if (result && result.location) {
        var panora = result;
        new google.maps.StreetViewPanorama(
          document.getElementById('pano'), {

            position: panora.location.latLng,
            pov: { heading: 0, pitch: -35 },

            motionTrackingControlOptions: {
              position: google.maps.ControlPosition.LEFT_BOTTOM
            },
            linksControl: true,
            panControl: true,
            enableCloseButton: true,
            fullscreenControl: false,
            addressControl: false,
            zoomControl: false,
            standAlone: true
          });

      }
      else if (this.nombre == 0) {
        this.setPanorama(0)
        this.nombre++
        console.log('vue 1')

      }
      else if (this.nombre == 1) {
        this.setPanorama(500)
        this.nombre++
        console.log('vue 2')

      }
      else if (this.nombre == 2) {
        this.setPanorama(1000)
        this.nombre++
        console.log('vue 3')

      }
      else if (this.nombre = 3) {
        this.setPanorama(5000)
        this.nombre++
        console.log('vue 4')
      }
      else if (this.nombre = 4) {
        this.setPanorama(10000)
        this.nombre++
        console.log('vue 4')
      }
    });
  }

  segmentChanged(event) {
    this.tab = event.value;
    this.cf.detectChanges();
  }


  checkActivite(value) {
    let activite = _.find(this.lake.lake_activite, function (o) { return o.activite_id[0]._id == value._id; });
    if (!activite)
      return { 'color': 'rgba(105, 113, 125, 0.2)' }

    if (activite.etat == 0)
      return { 'color': '#00aaf8' }

    if (activite.etat == 2)
      return { 'color': 'red' }

    if (activite.etat == 1)
      return { 'color': 'yellow' }

  }


}
