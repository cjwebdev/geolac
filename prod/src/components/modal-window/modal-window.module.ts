import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalWindowComponent } from './modal-window';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ModalWindowComponent),
  ],
  exports: [
    
  ]
})
export class ModalWindowComponentModule {}
