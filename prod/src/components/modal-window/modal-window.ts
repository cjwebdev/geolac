import { Component, Renderer, ChangeDetectorRef } from '@angular/core';
import { ViewController, NavParams, ModalController } from 'ionic-angular';
import { ExterneServiceProvider } from '../../providers/externe-service/externe-service';
import { ModalContentComponent } from '../modal-content/modal-content';


/**
 * Generated class for the ModalWindowComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'modal-window',
  templateUrl: 'modal-window.html'
})
export class ModalWindowComponent {
  public map;
  public text: any;
  public img: any;
  constructor(public renderer: Renderer, private cdRef: ChangeDetectorRef, public modal: ModalController, public externeService: ExterneServiceProvider, public viewCtrl: ViewController, public params: NavParams) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-window', true);
    this.map = params.get('map')
    this.text = this.params.get('info');

  }

  ngOnInit() {
    this.viewCtrl.didEnter.subscribe(() => {
      this.text = this.params.get('info');
      this.run();
    });
  }

  run() {
    this.externeService.getImagePlaceId(this.text.place_id, this.text.loc[1], this.text.loc[0]).then((data) => {
      this.img = data[0];
      this.cdRef.detectChanges();
    })
  }

  close() {
    this.viewCtrl.dismiss();
  }


  openContent(lake) {
    this.modal.create(ModalContentComponent, { info: lake }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'contentmodal' }).present()
  }
}
