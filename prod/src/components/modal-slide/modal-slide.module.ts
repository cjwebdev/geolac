import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalSlideComponent } from './modal-slide';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ModalSlideComponent),
  ],
  exports: [
    
  ]
})
export class ModalSlideComponentModule {}
