import { Component, Renderer, ChangeDetectorRef } from '@angular/core';
import { ViewController, NavParams, ModalController } from 'ionic-angular';
import { ExterneServiceProvider } from '../../providers/externe-service/externe-service';
import { ModalContentComponent } from '../modal-content/modal-content';
// import { listenToElementOutputs } from '@angular/core/src/view/element';
import { ModalWindowComponent } from '../modal-window/modal-window';

@Component({
  selector: 'modal-slide',
  templateUrl: 'modal-slide.html'
})
export class ModalSlideComponent {

  listLake: any;
  map: any;

  constructor(public renderer: Renderer, private cf: ChangeDetectorRef, public modal: ModalController, public externeService: ExterneServiceProvider, public viewCtrl: ViewController, public params: NavParams) {
    this.renderer.setElementClass(viewCtrl.pageRef().nativeElement, 'modal-slide', true);
    this.listLake = params.get('info');
    this.map = params.get('map');
    // this.run();
  }

<<<<<<< HEAD
  run() {
    this.listLake.forEach(resultRac => {
      this.externeService.getImagePlaceId(resultRac.place_id, resultRac.loc[1], resultRac.loc[0]).then((data) => {
        resultRac.img_link = data
        this.cf.detectChanges();
      })
    });
  }
=======
  // run() {
  //   this.listLake.forEach(resultRac => {
  //     this.externeService.getImagePlaceId(resultRac.place_id, resultRac.loc[1], resultRac.loc[0]).then((data) => {
  //       resultRac.img_link = data
  //     })
  //   });
  // }
>>>>>>> 6a864290d6310af4fd3d4245f738b92854ebb7f1
  close() {
    this.viewCtrl.dismiss();
  }
  locLake(lake) {
    let lakePos = { lat: lake.loc[1], lng: lake.loc[0] }
    console.log(lakePos);
    this.map.setCenter(lakePos);
    this.modal.create(ModalWindowComponent, { info: lake }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'geomodal' }).present();
  }
  openContent(lake) {
    if (lake == !lake)
      this.modal.create(ModalContentComponent, { info: lake }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'contentmodal' }).present()

    this.close()
  }
}
