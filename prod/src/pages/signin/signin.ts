import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

// Providers
import { AuthProvider } from '../../providers/auth/auth';
import { ToastProvider } from '../../providers/toast/toast';


// Pages
import { SignupPage } from '../signup/signup';
import { HomePage } from '../home/home';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {
  registerPage = SignupPage;
  password: string = '';
  username: string = '';

  constructor(public navCtrl: NavController, private loadCtrl: LoadingController,public toast: ToastProvider, private authPvdr: AuthProvider) { }

  ionViewDidLoad() {
  
  }

  public doSignin() {
    let loader = this.loadCtrl.create({
      content: 'Connexion en cours...'
    });
    loader.present();

    this.authPvdr.signin(this.username, this.password).subscribe((success) => {
      loader.dismiss();

      this.navCtrl.setRoot(HomePage);
    }, (error) => {
      loader.dismiss();
      this.toast.show('Identifiant ou mot de passe incorrecte', 2000, 'top');
    });
    loader.dismiss();
  }

}
