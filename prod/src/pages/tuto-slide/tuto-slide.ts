import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

// Pages
import { SigninPage } from '../signin/signin';
// import { HomePage } from '../home/home';

@Component({
  selector: 'page-tuto-slide',
  templateUrl: 'tuto-slide.html'
})
export class TutoSlidePage {
  registerPage = SigninPage;
  password: string = '';
  username: string = '';

  constructor(public navCtrl: NavController) { }

  ionViewDidLoad() {
  }

  public doSignin() {

      this.navCtrl.setRoot(SigninPage);
  }

}
