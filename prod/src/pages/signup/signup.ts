import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';

// Providers
import { AuthProvider } from '../../providers/auth/auth';
import { HomePage } from '../home/home';

// Pages

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html'
})
export class SignupPage {
  password: string = '';
  username: string = '';
  verify: string = '';
  email: string = '';

  constructor(public navCtrl: NavController, private authPvdr: AuthProvider, private loadCtrl: LoadingController) { }

  ionViewDidLoad() {
 
  }

  // TODO: form validation
  public doRegister() {
    let loader = this.loadCtrl.create({
      content: 'Connexion est cours ...'
    });
    loader.present();

    this.authPvdr.signup(this.username, this.password, this.email).subscribe((success) => {
      this.navCtrl.setRoot(HomePage);
      loader.dismiss();
    }, (error) => {
      loader.dismiss();
      alert('Identifiant, mot de passe ou email incorrecte');
    });
  }

}
