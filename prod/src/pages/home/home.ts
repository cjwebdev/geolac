import { Component, ViewChild, ElementRef } from '@angular/core';
import { App, NavController, ActionSheetController, ModalController, MenuController, Events } from 'ionic-angular';
import { Http } from '@angular/http';
import { ParseProvider } from '../../providers/parse/parse';
import { AuthProvider } from '../../providers/auth/auth';
import * as _ from 'lodash';
import { LocalisationProvider } from '../../providers/localisation/localisation';
import { ToastProvider } from '../../providers/toast/toast';
import { ModalContentComponent } from '../../components/modal-content/modal-content';
import { ModalSlideComponent } from '../../components/modal-slide/modal-slide';
import { LoadingProvider } from '../../providers/loading/loading';
import { ExterneServiceProvider } from '../../providers/externe-service/externe-service';
import { trigger, style, animate, transition } from '@angular/animations';
import { ContactPage } from '../../pages/contact/contact';
import { Storage } from '@ionic/storage';
import { setTimeout } from 'timers';
import {
  GoogleMaps,
  GoogleMap,
  LatLng,
  GoogleMapsEvent,
} from '@ionic-native/google-maps';
// import { ModalContentComponent } from '../../components/modal-content/modal-content';
// import { SearchListComponent } from '../../components/search-list/search-list';


declare var google: any;
declare var MarkerClusterer: any;

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('500ms', style({ transform: 'translateX(0)', opacity: 1 }))
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('500ms', style({ transform: 'translateX(100%)', opacity: 0 }))
        ])
      ]
    )
  ],
})
export class HomePage {
  @ViewChild('map') mapElement: ElementRef;

  newScore = { playerName: null, score: null };
  lakes = [];
  lake_act = []
  pushPage: any;
  public markers = [];
  public map;
  public markerCluster;
  public marker;
  public listLake;
  public defaultLake: any;
  public listLakeAct;
  public click;
  public openModal;
  public word;
  public search;
  public activite;
  public loadActivite;
  public json;
  public biketraffic;
  public roadtraffic;
  public clickSlide = false;
  constructor(private parseProvider: ParseProvider, public googleMaps: GoogleMaps, public events: Events, private storage: Storage, public menu: MenuController, public externeService: ExterneServiceProvider, public loader: LoadingProvider, public modal: ModalController, public toast: ToastProvider, public http: Http, public actionsheetCtrl: ActionSheetController, private auth: AuthProvider, private navCtrl: NavController, private app: App, public localisation: LocalisationProvider) {
    this.loader.show('')
    this.storage.get('lake').then((lakeCache) => {
      if (lakeCache) {
        this.defaultLake = lakeCache;
        this.listLake = lakeCache;
        this.initMap();
      } else {
        this.parseProvider.ObserveLake().subscribe((lake) => {
          this.defaultLake = lake.data;
          this.listLake = lake.data;
          this.initMap();
          this.storage.set('lake', lake.data);
        }, (error) => {
          // console.log(error);
        });
      }
    });
  }


  pushContact() {
    this.pushPage = ContactPage;
  }

  openMenu() {
    this.menu.open();
  }

  ngOnInit() {
  }

  initMap() {

    let element = this.mapElement.nativeElement;
    this.map = this.googleMaps.create(element);
    this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
      let options = {
        zoom: 8
      };
 
      this.map.moveCamera(options);
  });
}

  initMarker(result) {
    this.marker = [];
    for (let i = 0; result.length > i; i++) {
      let resultRac = result[i]
      var _latLng = { lat: Number(resultRac.loc[1]), lng: Number(resultRac.loc[0]) };
      var nom = resultRac.nom
      var marker = new google.maps.Marker({
        position: _latLng,
        map: this.map,
        icon: pinImage,
        shadow: pinShadow,
        title: nom
      });
      this.marker.push(marker);
      var pinColor = "00c6ff";
      var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=%E2%80%A2|" + pinColor,
        new google.maps.Size(21, 34),
        new google.maps.Point(0, 0),
        new google.maps.Point(10, 34));
      var pinShadow = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_shadow",
        new google.maps.Size(40, 37),
        new google.maps.Point(0, 0),
        new google.maps.Point(12, 35));
      google.maps.event.addListener(marker, 'click', (event) => {
        if (this.openModal) {
          this.openModal.dismiss();
        }

        // this.openModal = this.modal.create(ModalContentComponent, { info: result[i] }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'geomodal' });
        this.openModal = this.modal.create(ModalContentComponent, { info: result[i] }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'contentmodal' });

        this.openModal.present()
      });
      if (i == result.length - 1) {
        setTimeout(() => {
          this.loader.hide()
        }, 300);
      }
    }


    var mcOptions = { gridSize: 60, maxZoom: 20, imagePath: 'assets/images/m' };
    this.markerCluster = new MarkerClusterer(this.map, this.marker, mcOptions);
  }

  deleteMarker() {
    if (this.marker) {
      for (let i = 0; this.marker.length > i; i++) {
        this.marker[i].setMap(null)
      }
      this.marker = []
      this.markerCluster.clearMarkers()
    }
  }

  filterButton() {
    this.actionSheet();
  }

  actionSheet() {
    setTimeout(() => {
      let actionSheet = this.actionsheetCtrl.create({
        title: 'Option de la carte',
        cssClass: 'action-sheets-basic-page1',
        buttons: [
          {
            text: 'Lieux pour pêcher',
            icon: 'custom-fishing',
            handler: () => {
              this.filterSelect('fishing');
              this.toast.show("Lieux pour pêcher.", 5000, 'top');
            }
          },
          {
            text: 'Lieux pour naviguer',
            icon: 'boat',
            handler: () => {
              this.filterSelect('boating');
              this.toast.show("Lieux pour naviguer.", 5000, 'top');

            }
          },
          {
            text: 'Lieux pour se baigner',
            icon: 'custom-swim',
            handler: () => {
              this.filterSelect('swim');
              this.toast.show("Lieux pour se baigner.", 5000, 'top');

            }
          },
          {
            text: 'Tous les lieux',
            icon: 'custom-active-search-symbol',
            handler: () => {
              this.filterSelect('all');
              this.toast.show("Tous les lieux.", 5000, 'top');

            }
          },
          {
            text: 'Traffic routier en temps réel',
            icon: 'custom-road-perspective',
            handler: () => {
              this.filterSelect('traffic');
              this.toast.show("Traffic routier en temps réel.", 5000, 'top');

            }
          },
          {
            text: 'Piste cyclable',
            icon: 'custom-bicycle-rider',
            handler: () => {
              this.filterSelect('bike');
              this.toast.show("Piste cyclable pour la carte satellite et map.", 6000, 'top');

            }
          },
          {
            text: 'Quitter',
            role: 'cancel', // will always sort to be on the bottom
            icon: 'close',
            handler: () => {

            }
          }
        ]
      });
      actionSheet.present();
    }, 500);
  }

  filterSelect(select) {
    var array;
    this.loader.show('Chargement des lacs...')
    setTimeout(() => {
      if (this.click) {
        var array = this.defaultLake;
        this.click = false;

        if (this.biketraffic != false) {
          this.biketraffic.setMap(null)
          this.biketraffic = false;
        }
        if (this.roadtraffic != false) {
          this.roadtraffic.setMap(null)
          this.roadtraffic = false;
        }
      }


      if (select == 'fishing') {
        var array = this.listLake.filter((data, index) => {

          if (data.lake_activite && data.lake_activite[0] && data.lake_activite[0].activite_id[0]._id == "5bb131d5ed4e4fa0f6c2070d")
            return data;
        });
      } else if (select == 'swim') {
        var array = this.listLake.filter((data, index) => {
          if (data.lake_activite && data.lake_activite[0] && data.lake_activite[0].activite_id[0]._id == "5bb131cbed4e4fa0f6c206fe")
            return data;
        });
      } else if (select == 'boating') {
        var array = this.listLake.filter((data, index) => {
          if (data.lake_activite && data.lake_activite[0] && data.lake_activite[0].activite_id[0]._id == "5bb131e9ed4e4fa0f6c2071c")
            return data;
        });
      }
      else if (select == 'all') {
        var array = this.defaultLake;
      } else if (select == 'bike') {
        this.biketraffic = new google.maps.BicyclingLayer();
        this.biketraffic.setMap(this.map);
      } else if (select == 'traffic') {
        this.roadtraffic = new google.maps.TrafficLayer();
        this.roadtraffic.setMap(this.map);
      }
      this.loader.hide();
      this.deleteMarker();
      this.initMarker(array);
      this.click = true;
    }, 300)
  }

  clickLocalisation() {
    this.loader.show('Chargement de ta position...')
    this.localisation.run().then((data) => {
      if (data) {
        this.loader.hide();
        this.toast.show('Localisation réussie !', 5000, 'top');
        var myPos = { lat: this.localisation.latitude, lng: this.localisation.longitude }
        this.procheLakes(this.localisation.latitude, this.localisation.longitude, myPos);
        let options = {
          center: myPos,
          zoom: 14
        };
        this.map.setOptions(options);
      } else {
        this.loader.hide();
        this.toast.show("Localisation impossible. Merci d'activé votre GPS.", 5000, 'bottom');
      }
    });
  }

  addMarker(position, content, procheMy) {
    let markers = new google.maps.Marker({
      map: this.map,
      draggable: false,
      animation: google.maps.Animation.DROP,
      position: position
    });

    this.addInfoWindow(markers, procheMy);
    return markers;
  }

  addInfoWindow(markers, procheMy) {
    // var contentString = content;
    // let infoWindow = new google.maps.InfoWindow({
    //   maxWidth: 200,
    //   content: contentString,
    // });

    google.maps.event.addListener(markers, 'click', () => {
      var slide = this.modal.create(ModalSlideComponent, { info: procheMy, map: this.map }, { showBackdrop: false, enableBackdropDismiss: true, cssClass: 'slidemodal' });
      if (!this.clickSlide) {
        slide.present()
        this.clickSlide = true;
      } else {
        this.clickSlide = false;
      }
    });
  }

  procheLakes(lat1, lon1, myPos) {
    var pi = Math.PI;
    var R = 6371; //equatorial radius
    var distances = [];
    var closest = [];
    for (var i = 0; i < this.listLake.length; i++) {
      var lat2 = this.listLake[i].loc[1];
      var lon2 = this.listLake[i].loc[0];
      var chLat = lat2 - lat1;
      var chLon = lon2 - lon1;
      var dLat = chLat * (pi / 180);
      var dLon = chLon * (pi / 180);
      var rLat1 = lat1 * (pi / 180);
      var rLat2 = lat2 * (pi / 180);
      var a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(rLat1) * Math.cos(rLat2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
      var d = R * c;
      distances[i] = d;
      if (distances[i] < 50) {
        this.listLake[i].km = distances[i];
        closest.push(this.listLake[i]);
      }
      if (i == this.listLake.length - 1) {
        let procheMy = _.orderBy(closest, ['km']);
        this.toast.show('Vous êtes ici. Il y a ' + (procheMy.length - 1) + ' lacs à moins de 50km ! Lac le plus proche : ' + procheMy[0].name + '', 5000, 'middle')
        let center = new google.maps.LatLng(lat1, lon1);
        this.map.setCenter(center);
        this.search = false;
        let slide = this.modal.create(ModalSlideComponent, { info: procheMy, map: this.map }, { showBackdrop: true, enableBackdropDismiss: true, cssClass: 'slidemodal' });
        slide.present()
        this.addMarker(myPos, slide, procheMy);
      }


    }
  }

  addButtonMap(controlDiv, text, self) {
    var controlUI = document.createElement('div');
    controlUI.style.backgroundColor = '#fff';
    controlUI.style.border = '2px solid #fff';
    controlUI.style.borderRadius = '3px';
    controlUI.style.boxShadow = 'rgba(0, 0, 0, 0.3) 0px 1px 4px -1px';
    controlUI.style.cursor = 'pointer';
    controlUI.style.marginTop = '11px';
    controlUI.style.textAlign = 'center';
    controlUI.style.marginRight = '10px';
    controlUI.title = text;
    controlDiv.appendChild(controlUI);

    var controlText = document.createElement('div');
    controlText.style.color = 'rgb(25,25,25)';
    controlText.style.fontFamily = 'Roboto,Arial,sans-serif';
    controlText.style.fontSize = '10px';
    controlText.style.lineHeight = '33px';
    controlText.style.paddingLeft = '5px';
    controlText.style.paddingRight = '5px';
    controlText.innerHTML = text;
    controlUI.appendChild(controlText);

    // Setup the click event listeners: simply set the map to Chicago.
    controlUI.addEventListener('click', () => {
      if (text == 'Filtre')
        self.filterButton('option');
      else
        self.filterButton('legende');
    });
  }

  searchInput(event) {
    this.word = event;
  }
}