// Angular
import { Component } from '@angular/core';
import { Platform, Events } from 'ionic-angular';

// Ionic native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';
 import { AdMobFree, AdMobFreeBannerConfig } from '@ionic-native/admob-free';

//Initial page
// import { HomePage } from '../pages/home/home';
import { HomePage } from '../pages/home/home';

// Providers
import { AuthProvider } from '../providers/auth/auth';
import { ParseProvider } from '../providers/parse/parse';
import { LoadingProvider } from '../providers/loading/loading';
import * as _ from 'lodash';


@Component({
    templateUrl: 'app.html'
})
export class MyApp {
<<<<<<< HEAD
  rootPage: any = HomePage;
  rewardvideo: any;
  constructor(public platform: Platform, public loader: LoadingProvider, public events: Events, public statusBar: StatusBar, public splashScreen: SplashScreen, public parseProvider: ParseProvider, private storage: Storage) {
    platform.ready().then(() => {
      statusBar.hide();
      splashScreen.hide();
    });
  }
=======
    rootPage: any = HomePage;
    rewardvideo: any;
    constructor(public platform: Platform, private admobFree: AdMobFree, public loader: LoadingProvider, public events: Events, public statusBar: StatusBar, public splashScreen: SplashScreen, public parseProvider: ParseProvider, private storage: Storage) {
        platform.ready().then(() => {
            statusBar.hide();
            splashScreen.hide();
            this.init();
            this.showAdmobBannerAds();
        });
    }
    init() {
        var array = [];
        this.loader.show('');
        this.storage.get('lake').then((lakeCache) => {
            if (lakeCache) {
                this.events.publish('array', lakeCache, true);
            } else {
                this.parseProvider.ObserveLake().subscribe((lake) => {
                    this.events.publish('array', lake.data, true);
                    this.storage.set('lake', lake.data);
                }, (error) => {
                    // console.log(error);
                });
            }
        });
    }
    showAdmobBannerAds() {

        const bannerConfig: AdMobFreeBannerConfig = {
          id: 'ca-app-pub-2580762866650596/3131062005',
          isTesting: false,
          autoShow: true
        };
    
        this.admobFree.banner.config(bannerConfig);

        this.admobFree.banner.prepare()
          .then(() => {
           this.admobFree.banner.show()

            // banner Ad is ready
            // if we set autoShow to false, then we will need to call the show method here
          })
          .catch(e => console.log(e));
    }

>>>>>>> 6a864290d6310af4fd3d4245f738b92854ebb7f1

}
