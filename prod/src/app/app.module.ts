import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ionic2RatingModule } from 'ionic2-rating';

// App
import { MyApp } from './app.component';

// Native imports
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AdMobFree } from '@ionic-native/admob-free';
import { Geolocation } from '@ionic-native/geolocation';

//
import { GoogleMaps } from '@ionic-native/google-maps';



// Providers
import { ParseProvider } from '../providers/parse/parse';
import { AuthProvider } from '../providers/auth/auth';
import { ToastProvider } from '../providers/toast/toast';
import { LocalisationProvider } from '../providers/localisation/localisation';
import { ExterneServiceProvider } from '../providers/externe-service/externe-service';

// Pipe
import { SearchfilterPipe } from '../pipes/searchfilter/searchfilter';


// Pages
import { HomePage } from '../pages/home/home';
import { SigninPage } from '../pages/signin/signin';
import { SignupPage } from '../pages/signup/signup';
import { TutoSlidePage } from '../pages/tuto-slide/tuto-slide';
import { ContactPage } from '../pages/contact/contact';


// Components

import { ModalWindowComponent } from '../components/modal-window/modal-window';
import { ModalSlideComponent } from '../components/modal-slide/modal-slide';
import { ImagecacheDirective } from '../directives/imagecache/imagecache';
import { IonicStorageModule } from '@ionic/storage';
import { LoadingProvider } from '../providers/loading/loading';
import { SearchBarComponent } from '../components/search-bar/search-bar';
import { SearchListComponent } from '../components/search-list/search-list';
import { ModalContentComponent } from '../components/modal-content/modal-content';
import { ModalAddcomComponent } from '../components/modal-addcom/modal-addcom';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    TutoSlidePage,
    ContactPage,
    SearchfilterPipe,
    ModalWindowComponent,
    ModalSlideComponent,
    ImagecacheDirective,
    SearchBarComponent,
    SearchListComponent,
    ModalContentComponent,
    ModalAddcomComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { mode: 'md' }),
    HttpModule,
    IonicStorageModule.forRoot(),
    BrowserAnimationsModule,
    Ionic2RatingModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SigninPage,
    SignupPage,
    TutoSlidePage,
    ContactPage,
    ModalWindowComponent,
    ModalSlideComponent,
    ModalContentComponent,
    SearchListComponent,
    ModalAddcomComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ParseProvider,
    AuthProvider,
    GoogleMaps,
    LocalisationProvider,
    Geolocation,
    ToastProvider,
    ExterneServiceProvider,
    LoadingProvider,
    SearchfilterPipe,
    AdMobFree,
    ModalSlideComponent,
  ]
})
export class AppModule { }
