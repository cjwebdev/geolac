import { Pipe, PipeTransform } from '@angular/core';
import * as _ from 'lodash';


/**
 * Generated class for the SearchfilterPipe pipe.
 *
 * See https://angular.io/docs/ts/latest/guide/pipes.html for more info on
 * Angular Pipes.
 */
@Pipe({
  name: 'searchfilter',
})
export class SearchfilterPipe implements PipeTransform {
  /**
  * @param items object from array
  * @param term term's search
  */
  transform(items: any, term: any): any {
    if (!term || !items || term.type){
      return _.slice(items, 0, 30);
    }

    return SearchfilterPipe.filter(items, term);
  }

  /**
   *
   * @param items List of items to filter
   * @param term  a string term to compare with every property of the list
   *
   */
  static filter(items: Array<{ [key: string]: any }>, term: string): Array<{ [key: string]: any }> {
    const toCompare = term.toLowerCase();

    return items.filter(function (item: any) {
      for (let property in item) {
        if (item[property] === null || item[property] == undefined) {
          continue;
        }
        if (item[property].toString().toLowerCase().includes(toCompare)) {
          return true;
        }
      }
      return false;
    }).slice(0, 30);
  }
}
